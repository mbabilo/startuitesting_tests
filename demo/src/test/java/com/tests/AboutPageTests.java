package com.tests;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.testng.annotations.Test;


public class AboutPageTests extends TestBase{

    @Test
    public void testPageTitle() {
        app.goTo().homePage();
        app.goTo().aboutPage();
        String title = app.aboutPage().getPageTitle();
        MatcherAssert.assertThat(title, CoreMatchers.equalTo("about app"));
    }

    @Test
    public void testPageHeader() {
        app.goTo().homePage();
        app.goTo().aboutPage();
        String title = app.aboutPage().getSubtitleOfPage();
        MatcherAssert.assertThat(title,
                CoreMatchers.equalTo("This simple NodeJS application is written specifically for creating UI autotests."));
    }
}