package com.tests;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ContactsPageTests extends TestBase{

    @Test
    public void testPageTitle() {
        app.goTo().homePage();
        app.goTo().contactsPage();
        String title = app.contactsPage().getPageTitle();
        MatcherAssert.assertThat(title, CoreMatchers.equalTo("Contacts list"));
    }

    @Test
    public void testPageHeader() {
        app.goTo().homePage();
        app.goTo().contactsPage();
        String list = app.contactsPage().getContactsList();
        System.out.println(list);
        assertTrue(list.contains("Test User +11111111"));
    }
}