package com.tests;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.testng.annotations.Test;


public class HomePageTests extends TestBase {

    @Test
    public void testPageTitle() {
        app.goTo().homePage();
        String title = app.homePage().getPageTitle();
        MatcherAssert.assertThat(title, CoreMatchers.equalTo("mainPage"));
    }

    @Test
    public void testPageHeader() {
        app.goTo().homePage();
        String title = app.homePage().getHeaderOfPage();
        MatcherAssert.assertThat(title, CoreMatchers.equalTo("Welcome to the main page of test app."));
    }
}