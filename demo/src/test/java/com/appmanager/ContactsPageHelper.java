package com.appmanager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class ContactsPageHelper extends HelperBase{


    public ContactsPageHelper(WebDriver driver) {super(driver);}

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getContactsList() {
        return list(By.xpath("/html/body/div[1]/fieldset[2]"));
    }
}
