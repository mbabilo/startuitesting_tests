package com.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HelperBase {
    public WebDriver driver;

    public HelperBase(WebDriver driver) {
        this.driver = driver;
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public String title(By locator) {
        return driver.findElement(locator).getText();
    }

    public String list(By locator) {
        return driver.findElement(locator).getText();
    }

    public void open(String url) {
        driver.get(url);
    }
}
