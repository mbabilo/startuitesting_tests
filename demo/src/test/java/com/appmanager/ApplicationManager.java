package com.appmanager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;

import java.util.concurrent.TimeUnit;


public class ApplicationManager {
    public WebDriver driver;
    private String browser;
    private NavigationHelper navigationHelper;
    private AboutPageHelper aboutPageHelper;
    private HomePageHelper homePageHelper;
    private ContactsPageHelper contactsPageHelper;

    public ApplicationManager(String browser) {
        this.browser = browser;
    }

    public void init() {

        if (browser.equals(BrowserType.FIREFOX)) {
            System.setProperty("webdriver.gecko.driver", "F:\\issBlog\\How to get started with web UI automation\\startuitesting_tests\\geckodriver.exe"); // <-- Change this path
            driver = new FirefoxDriver();
        } else if (browser.equals(BrowserType.CHROME)) {
            System.setProperty("webdriver.chrome.driver", "F:\\issBlog\\How to get started with web UI automation\\startuitesting_tests\\chromedriver.exe"); // <-- Change this path
            driver = new ChromeDriver();
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://localhost:7000/");
        navigationHelper = new NavigationHelper(driver);
        aboutPageHelper = new AboutPageHelper(driver);
        homePageHelper = new HomePageHelper(driver);
        contactsPageHelper = new ContactsPageHelper(driver);
    }

    public void stop() {
        driver.quit();
    }
    public NavigationHelper goTo() {
        return navigationHelper;
    }

    public HomePageHelper homePage() {
        return homePageHelper;
    }

    public AboutPageHelper aboutPage() {
        return aboutPageHelper;
    }

    public ContactsPageHelper contactsPage() {
        return contactsPageHelper;
    }
}
