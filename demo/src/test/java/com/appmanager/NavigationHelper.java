package com.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NavigationHelper extends HelperBase {

    public NavigationHelper(WebDriver driver) {
        super(driver);
    }

    public void aboutPage() {
        click(By.xpath("/html/body/div/fieldset[1]/form/input"));
    }

    public void contactsPage() {
        click(By.xpath("/html/body/div/fieldset[2]/form/input"));
    }

    public void homePage() {
        open("http://localhost:7000/");
    }

}
