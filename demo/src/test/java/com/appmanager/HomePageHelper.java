package com.appmanager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePageHelper extends HelperBase{


    public HomePageHelper(WebDriver driver) {super(driver);}

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getHeaderOfPage() {
        return title(By.xpath("/html/body/h1"));
    }
}