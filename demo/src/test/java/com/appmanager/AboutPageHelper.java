package com.appmanager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class AboutPageHelper extends HelperBase{


    public AboutPageHelper(WebDriver driver) {super(driver);}

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getSubtitleOfPage() {
        return title(By.xpath("/html/body/h3"));
    }
}
